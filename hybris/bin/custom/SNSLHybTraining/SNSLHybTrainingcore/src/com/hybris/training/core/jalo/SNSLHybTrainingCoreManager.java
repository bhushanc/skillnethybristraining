/*
 * Copyright (c) 2019 SAP SE or an SAP affiliate company. All rights reserved.
 */
package com.hybris.training.core.jalo;

import de.hybris.platform.jalo.JaloSession;
import de.hybris.platform.jalo.extension.ExtensionManager;
import com.hybris.training.core.constants.SNSLHybTrainingCoreConstants;
import com.hybris.training.core.setup.CoreSystemSetup;


/**
 * Do not use, please use {@link CoreSystemSetup} instead.
 * 
 */
public class SNSLHybTrainingCoreManager extends GeneratedSNSLHybTrainingCoreManager
{
	public static final SNSLHybTrainingCoreManager getInstance()
	{
		final ExtensionManager em = JaloSession.getCurrentSession().getExtensionManager();
		return (SNSLHybTrainingCoreManager) em.getExtension(SNSLHybTrainingCoreConstants.EXTENSIONNAME);
	}
}
