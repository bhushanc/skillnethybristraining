/**
 *
 */
package com.hybris.training.core.handler;

import de.hybris.platform.servicelayer.model.attribute.DynamicAttributeHandler;

import java.time.LocalDate;
import java.time.Period;
import java.util.Calendar;

import com.hybris.training.core.model.MyCustomerModel;


/**
 * @author Bhushan.Chavan
 *
 */
public class MyCustomerCalculateAgeHandler implements DynamicAttributeHandler<Integer, MyCustomerModel>
{

	@Override
	public Integer get(final MyCustomerModel myCustomerModel)
	{
		return getCustomerAge(myCustomerModel);
	}

	@Override
	public void set(final MyCustomerModel var1, final Integer var2)
	{
		throw new UnsupportedOperationException();
	}

	/**
	 * @param myCustomerModel
	 */
	private Integer getCustomerAge(final MyCustomerModel myCustomerModel)
	{
		if (myCustomerModel.getBirthDate() != null)
		{
			final LocalDate today = LocalDate.now(); //Today's date
			final Calendar birthDayCal = Calendar.getInstance();
			birthDayCal.setTimeInMillis(myCustomerModel.getBirthDate().getTime());
			final LocalDate birthday = LocalDate.of(birthDayCal.get(Calendar.DATE), birthDayCal.get(Calendar.MONTH),
					birthDayCal.get(Calendar.YEAR)); //Birth date
			final Period p = Period.between(birthday, today);
			p.getYears();
		}
		return 0;
	}
}
