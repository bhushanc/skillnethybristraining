/*
 * ----------------------------------------------------------------
 * --- WARNING: THIS FILE IS GENERATED AND WILL BE OVERWRITTEN! ---
 * --- Generated at Jan 14, 2020, 3:22:09 PM                    ---
 * ----------------------------------------------------------------
 */
package com.hybris.training.fulfilmentprocess.constants;

/**
 * @deprecated since ages - use constants in Model classes instead
 */
@Deprecated
@SuppressWarnings({"unused","cast"})
public class GeneratedSNSLHybTrainingFulfilmentProcessConstants
{
	public static final String EXTENSIONNAME = "SNSLHybTrainingfulfilmentprocess";
	public static class Attributes
	{
		public static class ConsignmentProcess
		{
			public static final String DONE = "done".intern();
			public static final String WAITINGFORCONSIGNMENT = "waitingForConsignment".intern();
			public static final String WAREHOUSECONSIGNMENTSTATE = "warehouseConsignmentState".intern();
		}
	}
	
	protected GeneratedSNSLHybTrainingFulfilmentProcessConstants()
	{
		// private constructor
	}
	
	
}
