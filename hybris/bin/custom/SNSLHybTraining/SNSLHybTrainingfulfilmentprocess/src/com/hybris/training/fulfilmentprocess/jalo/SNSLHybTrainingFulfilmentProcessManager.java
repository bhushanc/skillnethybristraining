/*
 * Copyright (c) 2019 SAP SE or an SAP affiliate company. All rights reserved.
 */
package com.hybris.training.fulfilmentprocess.jalo;

import de.hybris.platform.jalo.JaloSession;
import de.hybris.platform.jalo.extension.ExtensionManager;
import com.hybris.training.fulfilmentprocess.constants.SNSLHybTrainingFulfilmentProcessConstants;

public class SNSLHybTrainingFulfilmentProcessManager extends GeneratedSNSLHybTrainingFulfilmentProcessManager
{
	public static final SNSLHybTrainingFulfilmentProcessManager getInstance()
	{
		ExtensionManager em = JaloSession.getCurrentSession().getExtensionManager();
		return (SNSLHybTrainingFulfilmentProcessManager) em.getExtension(SNSLHybTrainingFulfilmentProcessConstants.EXTENSIONNAME);
	}
	
}
