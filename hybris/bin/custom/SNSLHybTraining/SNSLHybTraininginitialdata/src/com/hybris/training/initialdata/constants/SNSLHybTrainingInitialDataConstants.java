/*
 * Copyright (c) 2019 SAP SE or an SAP affiliate company. All rights reserved.
 */
package com.hybris.training.initialdata.constants;

/**
 * Global class for all SNSLHybTrainingInitialData constants.
 */
public final class SNSLHybTrainingInitialDataConstants extends GeneratedSNSLHybTrainingInitialDataConstants
{
	public static final String EXTENSIONNAME = "SNSLHybTraininginitialdata";

	private SNSLHybTrainingInitialDataConstants()
	{
		//empty
	}
}
