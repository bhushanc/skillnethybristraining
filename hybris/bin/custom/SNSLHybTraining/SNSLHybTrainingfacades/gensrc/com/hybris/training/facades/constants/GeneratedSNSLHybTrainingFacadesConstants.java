/*
 * ----------------------------------------------------------------
 * --- WARNING: THIS FILE IS GENERATED AND WILL BE OVERWRITTEN! ---
 * --- Generated at Jan 14, 2020, 3:22:09 PM                    ---
 * ----------------------------------------------------------------
 */
package com.hybris.training.facades.constants;

/**
 * @deprecated since ages - use constants in Model classes instead
 */
@Deprecated
@SuppressWarnings({"unused","cast"})
public class GeneratedSNSLHybTrainingFacadesConstants
{
	public static final String EXTENSIONNAME = "SNSLHybTrainingfacades";
	
	protected GeneratedSNSLHybTrainingFacadesConstants()
	{
		// private constructor
	}
	
	
}
