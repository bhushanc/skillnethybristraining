/*
 * Copyright (c) 2019 SAP SE or an SAP affiliate company. All rights reserved.
 */
package com.hybris.training.facades.constants;

/**
 * Global class for all SNSLHybTrainingFacades constants.
 */
public class SNSLHybTrainingFacadesConstants extends GeneratedSNSLHybTrainingFacadesConstants
{
	public static final String EXTENSIONNAME = "SNSLHybTrainingfacades";

	private SNSLHybTrainingFacadesConstants()
	{
		//empty
	}
}
